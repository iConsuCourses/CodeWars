﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using static System.Console;

namespace Application
{
    public class Algoritmos
    {
        public static bool IsSquare(int n)
        {
            return (n >= 0 && Math.Sqrt(n) % 1 == 0) ? true : false;
        }

        public static int GetVowelCount(string str)
        {
            string vowel = "aeiou";
            int vowelCount = (from s in str join v in vowel on s equals v select v).Count();
            return vowelCount;
        }

        public static string Disemvowel(string str)
        {
            return Regex.Replace(str, "[aeiou]", "", RegexOptions.IgnoreCase);
        }

        public static string AddBinary(int a, int b) => Convert.ToString(a + b, 2);

        public static string DoubleChar(string s)
        {
            string str = "";
            for (int i = 0; i < s.Length; i++)
                for (int j = 0; j < 2; j++)
                    str += s[i];
            return str;
        }

        public static string FindNeedle(object[] haystack)
        {
            string str = "";
            for (int i = 0; i < haystack.Length; i++)
            {
                if (haystack[i] != null && haystack[i].GetType() == typeof(System.String) && Regex.IsMatch(haystack[i].ToString(), "(needle)", RegexOptions.IgnoreCase))
                {
                    str = "found the needle at position " + i;
                    break;
                }
            }
            return str;
        }

        public static string NumberToString(int num)
        {
            return num.ToString();
        }

        public static int CalculateYears(double principal, double interest, double tax, double desiredPrincipal)
        {
            if (principal < desiredPrincipal)
            {
                int years = 0;
                for (; principal <= desiredPrincipal; years++)
                    principal += ((principal * interest) - (principal * interest * tax));
                return years;
            }
            else
                return 0;
        }

        public static string[] SortByLength(string[] array)
        {
            Array.Sort(array, (x, y) => y.Length.CompareTo(x.Length));
            return array;
        }

        public static string ReverseWords(string str)
        {
            List<string> lista = str.Split(' ').ToList();
            str = string.Empty;
            foreach (string value in lista)
            {
                List<char> lista2 = value.ToList();
                lista2.Reverse();
                foreach (char value2 in lista2) str += value2;
                str += " ";
            }
            return str.Trim();
        }

        public static string BreakCamelCase(string str)
        {
            for (int i = 0; i < str.Length; i++) if (char.IsLetter(str[i]) && char.IsUpper(str[i])) str = str.Insert(str.IndexOf(str[i], i++), " ");
            return str;
        }

        public static string Remove_char(string s)
        {
            if (!string.IsNullOrEmpty(s))
            {
                s = s.Remove(0, 1);
                s = s.Remove(s.Length - 1, 1);
            }
            return s;
        }

        public static string Longest(string s1, string s2)
        {
            List<char> lista = (s1 + s2).ToList();
            List<char> lista2 = new List<char>();
            lista.Sort();
            foreach (char letter in lista) if (!lista2.Contains(letter)) lista2.Insert(lista2.Count, letter);
            return new string(lista2.ToArray());
        }

        public static string GetMiddle(string s)
        {
            if (s.Length % 2 == 0) return s.Substring((s.Length / 2) - 1, 2); else return s.Substring(((s.Length + 1) / 2) - 1, 1);
        }

        public static int[] distinct(int[] a) => a.ToList().Distinct().ToArray();

        public static string greet(string name) => (string.IsNullOrEmpty(name)) ? null : (name.Trim() == "") ? null : (Regex.IsMatch(name.ToLower(), @"^([a-z]+|[a-z]+\s?[a-z]+?)$")) ? "hello " + name + "!" : null;

        public static int Divisors(int n)
        {
            int sum = 0;
            for (int i = 1; i <= n; i++) if (n % i == 0) sum++;
            return sum;
        }

        public static string ToCamelCase(string str)
        {
            for (int i = 0; i < str.Length - 1; i++)
            {
                if (str[i] == '_' || str[i] == '-')
                {
                    str = str.Remove(i, 1);
                    str = str.Replace(str[i], char.ToUpper(str[i]));
                }
            }
            return str;
        }

        public static int MinimumNumber(int[] numbers)
        {
            //Número primo: Número natural mayor que 1 que tiene únicamente dos divisores distintos: él mismo y el 1
            int a = 0;
            List<int> list = numbers.ToList();
            while (list.Sum() % list.Sum() == 0 && list.Sum() % 1 == 0)
            {
                a++;
                list.Add(a);
            }
            return a;
        }

        public static int DigitalRoot(long n)
        {
            List<int> list = new List<int>();
            while (n.ToString().Length > 1)
            {
                list.Clear();
                foreach (char value in n.ToString()) list.Add(int.Parse(value.ToString()));
                n = list.Sum();
            }
            return Convert.ToInt16(n);
        }

        public static bool IsIsogram(string str)
        {
            str = str.ToLower();
            char[] array = str.ToCharArray();
            Array.Sort(array);
            for (int i = 0; i < array.Length - 1; i++)
            {
                if (array[i] == array[(i + 1)])
                {
                    return false;
                    break;
                }
            }
            return true;
        }

        public static bool ValidatePin(string pin) => Regex.IsMatch(pin, @"^(\d{6}|\d{4})$");

        public static string HighAndLow(string numbers)
        {
            List<int> list = new List<int>();
            foreach (string item in numbers.Split(' ')) list.Add(int.Parse(item));
            return list.Max() + " " + list.Min();
        }

        public static int Solution(int value)
        {
            int sum = 0;
            for (int i = 1; i < value; i++) if (i % 3 == 0 || i % 5 == 0) sum += i;
            return sum;
        }

        public static string Accum(string s)
        {
            for (int i = 0; i < s.Length; i++)
            {
                for (int j = 0; j < s.Length; j++)
                {

                }
            }
            return s;
        }

        public static string EvenOrOdd(int number) => (number % 2 == 0) ? "Even" : "Odd";

        public static string SongDecoder(string input) => (input.Length <= 0) ? "" : Regex.Replace(string.Join(" ", Regex.Split(input, "WUB")), @"\s{2,}", " ").Trim();

        public int[] FirstReverseTry(int[] arr)
        {
            if(arr.Length > 0)
            {
                int aux = arr[0];
                arr[0] = arr[(arr.Length - 1)];
                arr[(arr.Length - 1)] = aux;
                return arr;
            }
            else
            {
                return arr;
            }
        }

        public static int PositiveSum(int[] arr) => arr.Sum(x => (x > 0) ? x : 0);

        public static bool OnlyOne(params bool[] flags) => flags.Count(x => x) == 1;

        public static int[] TwoOldestAges(int[] ages)
        {
            if(ages != null && ages.Length > 1)
            {
                Array.Sort(ages);
                return new[] { ages[(ages.Length - 2)], ages[(ages.Length - 1)] };
            }
            else
            {
                return ages;
            }
        }

        public static double Round(double n) => Math.Round(n* 100) / 100;

        public static int SquareSum(int[] n)
        {
            int sum = 0;
            foreach (int item in n) sum += (int)Math.Pow(item, 2);
            return sum;
        }

        public static string ReverseLetter(string str)
        {
            return Regex.Replace(new string(str.Reverse().ToArray()), @"[^a-z]+", "");
        }

        public static string repeatStr(int n, string s)
        {
            string aux = s;
            while(n > 1)
            {
                s += aux;
                n--;
            }
            return s;
        }

        public static ulong OddCount(ulong n)
        {
            ulong sum = 0;
            for (ulong i = 1; i < n; i++) if (i % 2 != 0) sum++;
            return sum;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            WriteLine("1: " + Algoritmos.SquareSum(new int[] { 1, 2, 2 }));
            WriteLine("2: " + Algoritmos.SquareSum(new int[] { 1, 2 }));
            WriteLine("3: " + Algoritmos.SquareSum(new int[] { 5, 3, 4 }));
            WriteLine("4: " + Algoritmos.SquareSum(new int[] { 1, 2, 10 }));
            ReadKey();
        }
    }
}
